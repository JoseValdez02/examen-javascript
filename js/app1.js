// Obtener los elementos del formulario
const txtNumBoleto = document.getElementById("txtNumBoleto");
const txtNombre = document.getElementById("txtNombre");
const txtDestino = document.getElementById("txtDestino");
const cmbViajes = document.getElementById("cmbViajes");
const txtPrecio = document.getElementById("txtPrecio");
const txtSubtotal = document.getElementById("txtSubtotal");
const txtImpuesto = document.getElementById("txtImpuesto");
const txtTotal = document.getElementById("txtTotal");
const btnCalcular = document.getElementById("btnCalcular");
const btnLimpiar = document.getElementById("btnLimpiar");


// Agregar evento al botón de calcular
btnCalcular.addEventListener("click", function() {
  // Obtener los valores ingresados por el usuario
  const numBoleto = txtNumBoleto.value;
  const nombre = txtNombre.value;
  const destino = txtDestino.value;
  const tipoViaje = cmbViajes.value;
  const precio = parseFloat(txtPrecio.value);

  // Calcular el precio final según el tipo de viaje
  let precioFinal = precio;
  if (tipoViaje === "2 : Doble") {
    precioFinal *= 1.8; // Incremento del 80% para viaje doble
  }

  // Calcular el subtotal, impuesto y total
  const subtotal = precioFinal;
  const impuesto = subtotal * 0.16;
  const total = subtotal + impuesto;

  // Mostrar los resultados en los campos correspondientes
  txtSubtotal.value = subtotal.toFixed(2);
  txtImpuesto.value = impuesto.toFixed(2);
  txtTotal.value = total.toFixed(2);
});

// Agregar evento al botón de limpiar
btnLimpiar.addEventListener("click", function() {
  // Limpiar los campos del formulario
  txtNumBoleto.value = "";
  txtNombre.value = "";
  txtDestino.value = "";
  cmbViajes.value = "1 : Sencillo";
  txtPrecio.value = "";
  txtSubtotal.value = "";
  txtImpuesto.value = "";
  txtTotal.value = "";
});

