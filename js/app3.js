// Obtener los elementos del DOM
const btnGenerar = document.getElementById("btnGenerar");
const btnLimpiar = document.getElementById("btnLimpiar");
const edadesElement = document.getElementById("edades");
const bebesElement = document.getElementById("bebes");
const ninosElement = document.getElementById("ninos");
const adolescentesElement = document.getElementById("adolescentes");
const adultosElement = document.getElementById("adultos");
const ancianosElement = document.getElementById("ancianos");

// Agregar evento al botón de generar
btnGenerar.addEventListener("click", function() {
  // Generar las edades aleatorias
  const edades = generarEdades();

  // Mostrar las edades en el párrafo separadas por comas
  edadesElement.textContent = edades.join(", ");

  // Calcular y mostrar la cantidad de personas en cada rango de edad
  const bebes = contarPersonasEnRango(edades, 1, 3);
  const ninos = contarPersonasEnRango(edades, 4, 12);
  const adolescentes = contarPersonasEnRango(edades, 13, 17);
  const adultos = contarPersonasEnRango(edades, 18, 60);
  const ancianos = contarPersonasEnRango(edades, 61, 100);

  bebesElement.textContent = bebes;
  ninosElement.textContent = ninos;
  adolescentesElement.textContent = adolescentes;
  adultosElement.textContent = adultos;
  ancianosElement.textContent = ancianos;
});

// Agregar evento al botón de limpiar
btnLimpiar.addEventListener("click", function() {
  // Limpiar los resultados y el párrafo de edades
  edadesElement.textContent = "";
  bebesElement.textContent = "";
  ninosElement.textContent = "";
  adolescentesElement.textContent = "";
  adultosElement.textContent = "";
  ancianosElement.textContent = "";
});

// Función para generar las edades aleatorias
function generarEdades() {
  const edades = [];
  for (let i = 0; i < 100; i++) {
    const edad = Math.floor(Math.random() * 91); // Generar un número aleatorio entre 0 y 90
    edades.push(edad);
  }
  return edades;
}

// Función para contar la cantidad de personas en un rango de edades
function contarPersonasEnRango(edades, min, max) {
  let contador = 0;
  for (let i = 0; i < edades.length; i++) {
    if (edades[i] >= min && edades[i] <= max) {
      contador++;
    }
  }
  return contador;
}