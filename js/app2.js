// Obtener los elementos del formulario
const txtGrados = document.getElementById("txtGrados");
const cmbConvertidor = document.getElementById("cmbConvertidor");
const txtResultados = document.getElementById("txtResultados");
const btnCalcular = document.getElementById("btnCalcular");
const btnLimpiar = document.getElementById("btnLimpiar");

// Agregar evento al botón de calcular
btnCalcular.addEventListener("click", function() {
  // Obtener los valores ingresados por el usuario
  const grados = parseFloat(txtGrados.value);
  const convertidor = cmbConvertidor.value;

  // Realizar la conversión según la opción seleccionada
  let resultado;
  if (convertidor === "Celsius a Fahrenheit") {
    resultado = (grados * 9/5) + 32;
  } else {
    resultado = (grados - 32) * 5/9;
  }

  // Mostrar el resultado en el campo
  txtResultados.value = resultado.toFixed(2);
});

// Agregar evento al botón de limpiar
btnLimpiar.addEventListener("click", function() {
  txtGrados.value = "";
  cmbConvertidor.value = "Celsius a Fahrenheit";
  txtResultados.value = "";
});